import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JokesComponent } from './sections/jokes/jokes.component';
import { SectionsComponent } from './sections/sections.component';
import { ToolbarComponent } from './sections/toolbar/toolbar.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './themes/material/material.module';
import { DialogComponent } from './utilities/dialog/dialog.component';
import { SpinnerComponent } from './utilities/spinner/spinner.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    JokesComponent,
    SectionsComponent,
    ToolbarComponent,
    DialogComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
