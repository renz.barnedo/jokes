import { Component, HostListener, OnInit } from '@angular/core';
import { JokesService } from 'src/app/services/jokes.service';

interface Joke {
  joke: string;
  imageUrl: string;
}

@Component({
  selector: 'app-jokes',
  templateUrl: './jokes.component.html',
  styleUrls: ['./jokes.component.scss'],
})
export class JokesComponent implements OnInit {
  jokes: Joke[] = [];
  showUi: boolean = false;
  dadJokesPageNumber: number = 1;
  addJokeRuns: number = 0;
  warning: string =
    "WARNING: All of these jokes came from APIs. I'm not responsible for any joke that is offensive because I didn't make any of these. Scroll to the bottommost part to load more jokes...";

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const position: number =
      (document.documentElement.scrollTop || document.body.scrollTop) +
      document.documentElement.offsetHeight;
    const max: number = document.documentElement.scrollHeight;
    if (position == max) {
      this.addJokeRuns++;
      if (this.addJokeRuns > 1) {
        return;
      }
      this.addJokes();
    }
  }

  constructor(private jokesService: JokesService) {}

  async ngOnInit() {
    try {
      this.showUi = false;
      await this.addJokes();
      this.showUi = true;
    } catch (error) {
      this.jokesService.openDialog('Error', 'Cant initialize, please reload.');
    }
  }

  async addJokes() {
    this.jokesService.openLoadingSpinner();
    await this.getFromJokeApi();
    await this.getDadJokes();
    await this.getChuckNorrisJoke();
    await this.setJokeAndPhoto();
    this.jokesService.closeLoadingSpinner();
    this.addJokeRuns = 0;
  }

  async getFromJokeApi() {
    try {
      const response: any = await this.jokesService.getFromJokeApi();
      response.jokes.forEach((joke: any) => {
        joke = joke.joke
          ? `(${joke.category})  ${joke.joke}`
          : `(${joke.category}) ${joke.setup} ${joke.delivery}`;
        const fullJoke = {
          joke,
          imageUrl: '',
        };
        this.jokes.push(fullJoke);
      });
    } catch (error) {
      this.jokesService.openDialog('Error', 'Cant get jokes1, please reload.');
    }
  }

  async getDadJokes() {
    try {
      this.dadJokesPageNumber++;
      const response: any = await this.jokesService.getDadJokes(
        this.dadJokesPageNumber
      );
      response.results.forEach((result: any) =>
        this.jokes.push({
          joke: result.joke,
          imageUrl: '',
        })
      );
    } catch (error) {
      this.jokesService.openDialog('Error', 'Cant get jokes2, please reload.');
    }
  }

  async getChuckNorrisJoke() {
    try {
      const response: any = await this.jokesService.getChuckNorrisJoke();
      this.jokes.push({
        joke: response.value,
        imageUrl: '',
      });
    } catch (error) {
      this.jokesService.openDialog('Error', 'Cant get jokes3, please reload.');
    }
  }

  async setJokeAndPhoto() {
    try {
      this.jokes = this.jokes.map((joke: Joke, index: number) => {
        joke.imageUrl = `https://picsum.photos/500?random=${index}`;
        return joke;
      });
    } catch (error) {
      this.jokesService.openDialog('Error', 'Cant set photos, please reload.');
    }
  }
}
