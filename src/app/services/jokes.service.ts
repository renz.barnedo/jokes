import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../utilities/dialog/dialog.component';
import { SpinnerComponent } from '../utilities/spinner/spinner.component';

@Injectable({
  providedIn: 'root',
})
export class JokesService {
  defaultOptions: any = { headers: { Accept: 'application/json' } };
  constructor(private http: HttpClient, private dialog: MatDialog) {}

  getFromJokeApi() {
    let apiUrl = 'https://v2.jokeapi.dev';
    apiUrl += '/joke/any?amount=7';
    const options: any = {};
    return this.getPromiseFromResponse(apiUrl, options);
  }

  getDadJokes(page: number) {
    let apiUrl = 'https://icanhazdadjoke.com';
    apiUrl += `/search?page=${page}&limit=6`;
    const options: any = this.defaultOptions;
    return this.getPromiseFromResponse(apiUrl, options);
  }

  getChuckNorrisJoke() {
    let apiUrl = 'https://api.chucknorris.io/jokes';
    apiUrl += '/random';
    const options: any = {};
    return this.getPromiseFromResponse(apiUrl, options);
  }

  getPromiseFromResponse(apiUrl: string, options: any) {
    return new Promise((resolve, reject) =>
      this.http.get(apiUrl, options).subscribe(
        (response: any) => resolve(response),
        (error: any) => reject(error)
      )
    );
  }

  openLoadingSpinner() {
    this.dialog.open(SpinnerComponent, {
      disableClose: true,
      panelClass: 'transparent',
    });
  }

  closeLoadingSpinner() {
    this.dialog.closeAll();
  }

  openDialog(title: string, message: string) {
    this.dialog.open(DialogComponent, {
      data: {
        title,
        message,
      },
    });
  }
}
